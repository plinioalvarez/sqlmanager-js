  function consultasjs( qry, fn ){  
  var url = 'http://10.244.199.131:8000/api/helper/find?query='; 
   $.get( url + qry, function(record) {
       fn( record.data );
       return record.data;
      });
    };

 function cargar_tablas() {
  var qry = 'SHOW FULL TABLES FROM maxmonitor';
 var array = consultasjs( qry,
    function (data) {
     var array = [];     
    for (index in data) {   
          var arr = new Array;
          arr.push(data[index].Tables_in_maxmonitor);
          array.push(arr);
       }; 
      array.sort();
      array.unshift('Escoja tabla a consultar...');
      addOptions("tablas", array);
    }); 
}

function cargar_campos(idvalue) {
  $("#campostable").val("");  
  var tabla = $("#"+idvalue+" option:selected").text();  
  var qry = 'SHOW COLUMNS FROM '+tabla;
 var array = consultasjs( qry,
    function (data) {
     var array = [];   
    for (index in data) {
          var arr = new Array;
          arr.push(data[index].Field);
          array.push(arr);
       }; 
     // array.sort();
      array.unshift('*');
      addOptions("campos", array);
      $("#campostable").val(array.toString().replace("*,",""));
      buscaprimarykey(tabla);
    }); 
}

function ejecuta_consulta(qry) {    
    $("#result").html("");
    var array = consultasjs( qry, function(data) {
    var tFoot =  document.createElement("tfoot");
    var tF_row = tFoot.insertRow();
       if (data !== undefined){
         var cell = tF_row.insertCell();
         cell.colSpan = Object.keys(data[0]).length;
         cell.align = "center";
         cell.appendChild(document.createTextNode(data.length+' Registro(s)'));
         $("#count").text(data.length+' Registro(s)');
    
    var arrayOfThisHead = [];     
	var tHead = document.createElement("thead");	
    var tH_row = tHead.insertRow();
     $.each(data[0], function(k, v) {
            var cell = tH_row.insertCell();
            cell.appendChild(document.createTextNode(k));         
            arrayOfThisHead.push(k);
        })        
    var tbl_body = document.createElement("tbody");
    var odd_even = true;  
     $.each(data, function() {         
        var tbl_row = tbl_body.insertRow();
        tbl_row.className = odd_even ? "odd" : "even";
     $.each(this, function(k, v) {
            var cell = tbl_row.insertCell();
          cell.appendChild(document.createTextNode(v));
        })        
        odd_even = !odd_even;               
    })    
    $("#result").append(tHead);
    $("#result").append(tbl_body);
//  $("#result").append(tFoot);

 $('#result tr').on('click', function(){
   var dato = $(this).find('td').html();
   $("#primarykeyval").val(dato);
   var arrayOfThisRow = [];
   var tableData = $(this).find('td');
   var num = '';
   if (tableData.length > 0) {
       tableData.each(function() { 
        num = $(this).text();  
        if (isNaN(num)) {  
            arrayOfThisRow.push("'"+num+"'");
        } else
            arrayOfThisRow.push(num);             
       }); 
    } 
    var Row1 =   arrayOfThisRow;
    var head1 =  arrayOfThisHead;      
    var updatecad = '';
    var insertcad1 = '';
    var insertcad2 = '';
    var deletecad = '';
    var tabla = $("#tablas option:selected").text();
    for ( val in head1 ) {
      if (updatecad == '') { 
      updatecad = ""+head1[val]+" = "+Row1[val];
      insertcad1 = head1[val];
      insertcad2 = Row1[val];
    } else  {
      updatecad = updatecad + ", \n"+head1[val]+" = "+Row1[val];
      insertcad1 = insertcad1 + ", "+head1[val];
      insertcad2 = insertcad2 + ", "+Row1[val]; }
    }
    updatecad = "Update "+tabla+" set \n"+ updatecad +" \n"+ 
                "where "+$("#primarykey").val()+" = "+$("#primarykeyval").val(); 
    insertcad1 = "Insert into "+tabla+" \n"+
                 "("+insertcad1+") \n VALUES \n("+insertcad2+")"            
    deletecad = "Delete from "+tabla+" \n"+
                "where "+$("#primarykey").val()+" = "+$("#primarykeyval").val(); 

    if ($("button.selected").html() == "Update") {
       $("#Query").val( updatecad );
    } else
    if ($("button.selected").html() == "Insert") {
       $("#Query").val( insertcad1 );
    } else
    if ($("button.selected").html() == "Delete") {
       $("#Query").val( deletecad );
    }   
  });
} else
$("#count").text(data.length+' No se encontraron Registro(s)');
});   };

function consulta() {
    var campos = $("#campos option:selected").map(function() {
        return this.value;
    }).get().join(","); 
if (campos==""){
alert('Elija los campos a consultar');
};            
var tabla = $("#tablas option:selected").text();
var qry = "select "+campos+" from "+tabla;
if ($("#Where").val() != "") {
qry = qry + " Where "+$("#Where").val();
}
 ejecuta_consulta(qry);
}

function crearconsulta(value) {
    if (value == "Execute"){
      ejecuta_consulta($("#Query").val());
    } else
    $("#Query").html("");    
    var campos = $("#campos option:selected").map(function() {
                    return this.value;
                }).get().join(",");            
    var tabla = $("#tablas option:selected").text();
    var qry = "";
    var key1 = $("#primarykey").val();
    if (campos == '*') {
        campos =  $("#campostable").val();
     } 
    if (value == "Clear"){
       qry = "";
    } else
    if (campos==""){
        qry = "Favor elegir los campos a consultar..."
        } else  
    if (value == "Select"){
        qry = "SELECT \n "+campos+" \nFROM \n "+tabla+" \nWHERE "+key1+" = "+key1;
    } else    
    if (value == "Update"){
       var upd = "";
       var array = campos.split(",");       
       for (val in array){
         if (upd == "") {
            upd = array[val] +" = :"+array[val]  
         } else 
            upd = upd +',\n '+ array[val] +" = :"+array[val] 
       } 
      qry = "UPDATE "+tabla+" SET \n " +upd+" \nWHERE "+key1+" = "+key1;
    } else
    if (value == "Insert"){
       var ins = "";
       var array = campos.split(","); 
       for (val in array){
        if (ins == "") {  
          ins = ":"+array[val] 
        } else
        ins = ins+",:"+array[val]   
         } 
      qry = "INSERT INTO "+tabla+" \n (" +campos+") \nVALUES \n ("+ins+")";
    } else
    if (value == "Delete"){
      qry = "DELETE FROM "+tabla+" \nWHERE "+key1+" = "+key1;
    } 

   $("#Query").val(qry);  
}
// Rutina para agregar opciones a un <select>
function addOptions(domElement, array) {
 var select = document.getElementsByName(domElement)[0];
 select.innerHTML = "";
 for (value in array) {
  var option = document.createElement("option");
  option.text = array[value];
  select.add(option);
 }
}

function buscaprimarykey(tabla) {
  var qry = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "+
            "WHERE table_name = '"+tabla+"'"+
            " AND COLUMN_KEY = 'PRI' ";
   consultasjs( qry, function (data) {
         $("#primarykey").val( data[0].COLUMN_NAME ); 
     }); 
}

$('button.btnselect').on('click', function(){    
    $('button.selected').removeClass('selected');
    $(this).addClass('selected');   
});